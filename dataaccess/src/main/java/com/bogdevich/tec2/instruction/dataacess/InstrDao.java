package com.bogdevich.tec2.instruction.dataacess;

import com.bogdevich.tec2.instruction.dataacess.filter.InstrFilter;
import com.bogdevich.tec2.instruction.datamodel.Instr;

public interface InstrDao extends AbstractDao<Instr, Long, InstrFilter>{

}
